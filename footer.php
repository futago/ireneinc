            <footer>
                <div id="footer-inner">
                    <p><strong>Ireneinc Planning</strong> 49 Tasma Street, North Hobart Tasmania 7000 P. (03) 6234 9281 E. <a href="mailto:planning@ireneinc.com.au">planning@ireneinc.com.au</a></p>
                    <p>Copyright IreneInc 2015 Designed and built by <a href="http://www.futago.com.au" target="_blank">futago</a></p>
                </div>
            </footer>
        </div>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <?php wp_footer(); ?>
    </body>
</html>