<?php get_header(); ?>
            <article id="main-content">
              <div class="page-top">
                <h1><?php echo the_title(); ?></h1>
                <div class="page-top-image">
                  <?php 
                    if ( has_post_thumbnail() ) {
                      the_post_thumbnail();
                    } 
                  ?>
                </div>
              </div>
                <?php
                    if (have_posts()) :
                       while (have_posts()) :
                          the_post();
                             the_content();
                       endwhile;
                    endif;
                ?>
            </article>
<?php get_footer(); ?>