<?php get_header(); ?>
            <article id="main-content" class="page">
              <div class="page-top">
                <h1><?php echo the_title(); ?></h1>
                
                  <?php 
                    if ( has_post_thumbnail() ) {
                      echo '<div class="page-top-image">';
                      the_post_thumbnail('large');
                      echo '</div>';
                    } 
                  ?>

              </div>
                <?php
                    if (have_posts()) :
                       while (have_posts()) :
                          the_post();
                             the_content();
                       endwhile;
                    endif;
                ?>
            </article>
<?php get_footer(); ?>