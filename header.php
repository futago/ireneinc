<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php wp_title(); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">

        <link href='http://fonts.googleapis.com/css?family=Asap|Source+Sans+Pro:600,400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
        <?php wp_head(); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="outer">
            <div id="header">
                <div id="header-inner">
                    <div id="header-logo"><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/header-logo.png" alt="IreneInc" /></a></div>
                    <a id="mobile-menu-link" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/menu-burger.png" alt="" />Menu</a>
                    <nav id="menu">
                        <a href="#" class="close"> close</a>
                       <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
                    </nav>
                </div>
            </div>