				<div class="comments">
				<h2>Comments</h2>
                <?php if($comments) : ?>
				  	<?php foreach($comments as $comment) : ?>
						<div class="comment">
							<?php if($comment->comment_approved == '0') : ?>
								<p>Your comment is awaiting approval</p>
							<?php endif; ?>
							<p><strong>
								<?php comment_author(); ?>
							</strong></p>
							<p>
								<?php comment_text(); ?>
							</p>
							<p class="date">
							<?php comment_date(); ?> at <?php comment_time(); ?>
							</p>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<p>No-one has made any comments yet</p>
				<?php endif; ?>

				<?php 

				$fields =  array(

				  'author' =>
				    '<p class="comment-form-author"><label for="author">' . __( 'Name', 'domainreference' ) . '</label> ' .
				    ( $req ? '<span class="required">*</span>' : '' ) .
				    '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
				    '" size="30"' . $aria_req . ' /></p>',

				  'email' =>
				    '<p class="comment-form-email"><label for="email">' . __( 'Email', 'domainreference' ) . '</label> ' .
				    ( $req ? '<span class="required">*</span>' : '' ) .
				    '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
				    '" size="30"' . $aria_req . ' /></p>',

				  'url' => '',
				);

				$args = array(
					'fields' => apply_filters( 'comment_form_default_fields', $fields )
				);
				comment_form( $args, $post_id );
				?>

              </div>