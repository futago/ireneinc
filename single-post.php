<?php get_header(); ?>
            <article id="main-content" class="blog page">
              <div class="page-top">
                <h1><span class="pink">Blog</span> / <?php echo the_title(); ?></h1>
                <div class="page-top-image">
                  <?php 
                    if ( has_post_thumbnail() ) {
                      the_post_thumbnail();
                    } 
                  ?>
                </div>
              </div>
              <div class="outer">
              <div class="left">
                <p class="category">
                  <?php $my_cats = get_the_category();
                    $num = count($my_cats);
                    $c = 0;
                    foreach( $my_cats as $my_cat ) {
                      $c++;
                      echo $my_cat->cat_name;
                      if( $c < $num ) echo ', ';
                    }
                  ?>
                </p>
                <h2 class="title"><?php echo the_title(); ?></h2>
                <p class="date"><?php the_date(); ?></p>
                <div class="project-content">
                  <?php
                      if (have_posts()) :
                         while (have_posts()) :
                            the_post();
                               the_content();
                               comments_template();
                         endwhile;
                      endif;
                  ?>
                </div>
              </div>
              <div class="right">
                <?php

                if( have_rows('related_posts') ):

                  ?>

                  <h2>Related posts</h2>

                  <?php

                    while ( have_rows('related_posts') ) : the_row();

                        $post = get_sub_field('related_post');
                        ?>
                          <div class="related">
                            <a href="<?php echo get_permalink($post->ID); ?>"><?php if(has_post_thumbnail($post->ID) ){
                              echo get_the_post_thumbnail( $post->ID, 'square' ); 
                            } ?><h3><?php the_title($post->ID); ?></h3>
                            </a>
                            <p class="date"><?php echo get_the_date(); ?></p>
                          </div>
                        <?php

                    endwhile;

                else :
                	?><h2>Related posts</h2><?php
                	$current_post = get_the_ID ();
                    query_posts('post_type=post&posts_per_page=2');
                	while ( have_posts() ) : the_post(); 
                	if ($post->ID != $current_post):
                	?>
                		<div class="related">
                            <a href="<?php echo get_permalink($post->ID); ?>"><?php if(has_post_thumbnail($post->ID) ){
                              echo get_the_post_thumbnail( $post->ID, 'square' ); 
                            } ?><h3><?php echo get_the_title($post->ID); ?></h3>
                            </a>
                            <p class="date"><?php echo get_the_date(); ?></p>
                          </div>
                	<?php 
                	endif;
                	endwhile;

                endif;

                ?>
              </div>
              </div>
            </article>
<?php get_footer(); ?>