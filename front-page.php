<?php get_header(); ?>
<h1 class="off-left">IreneInc urban planning consultants</h1>
            <article id="main-content" class="home">
                <div class="intro">
                  <div class="text">
                    <?php the_field('intro_text'); ?>
                  </div>
                  <div class="link">
                    <a href="<?php the_field('intro_link_page'); ?>"><?php the_field('intro_link_text'); ?></a>
                  </div>
                </div>
                <div class="feature">
                  <?php
                  $latest_post = wp_get_recent_posts(array('post_type'=>'irene_projects', 'posts_per_page'=>1, 'orderby'=>'menu_order'));
                  ?>
                  <?php $image = get_the_post_thumbnail($latest_post[0]['ID'], 'home-feature');
                  echo $image; ?>
                  <div class="text">
                    <h2><?php echo get_the_title($latest_post[0]['ID']); ?></h2>
                    <p><?php echo get_the_excerpt($latest_post[0]['ID']); ?></p>
                    <p><a href="<?php echo get_the_permalink($latest_post[0]['ID']); ?>">Read the case study</a></p>
                  </div>
                </div>
                <div class="boxes">
                  <div class="left-box">
                    <div class="img">
                      <?php $image = get_field('left_box_image'); ?>
                      <img src="<?php echo $image['sizes']['square'] ?>" alt="" />
                    </div>
                    <div class="text">
                      <?php the_field('left_box_text'); ?>
                    </div>
                  </div>
                  <div class="right-box">
                  <?php
                    query_posts('post_type=post&posts_per_page=1&orderby="date"');
                    while ( have_posts() ) : the_post();
                      echo '<div class="img">';
                        if ( has_post_thumbnail() ) {
                        the_post_thumbnail('square');
                      } 
                      echo '</div>';
                        echo '<div class="text"><h3>';
                        the_title();
                        echo '</h3><div class="link"><a href="'.get_the_permalink().'">Latest blog post</a></div>';
                        echo '</div>';
                    endwhile;
                    wp_reset_query();
                  ?>
                  </div>
                </div>
            </article>
<?php get_footer(); ?>