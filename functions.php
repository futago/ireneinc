<?php

add_filter('show_admin_bar', '__return_false');

add_theme_support( 'post-thumbnails' );

add_image_size( 'home-feature', 1151, 496, true );
add_image_size( 'square', 800, 800, true );
add_image_size( 'small-square', 400, 400, true );
add_image_size( 'button', 521, 600, true );

// Load scripts

function irene_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.8.3.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr');

		wp_register_script('plugin-scripts', get_template_directory_uri() . '/js/plugins.js', array('jquery'), filemtime( get_template_directory() . '/js/scripts.js' ), true); // Custom scripts
        wp_enqueue_script('plugin-scripts');
        
        wp_register_script('irene-scripts', get_template_directory_uri() . '/js/main.js', array('jquery'), filemtime( get_template_directory() . '/js/scripts.js' ), true); // Custom scripts
        wp_enqueue_script('irene-scripts');
        
    }
}

add_action('init', 'irene_header_scripts');

// Register menus

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

// Register Custom Post Type
function set_irene_projects() {

	$labels = array(
		'name'                => _x( 'Irene Projects', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Project', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Irene Projects', 'text_domain' ),
		'name_admin_bar'      => __( 'Projects', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Items', 'text_domain' ),
		'add_new_item'        => __( 'Add New Item', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Item', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'irene_projects', 'text_domain' ),
		'description'         => __( 'IreneInc projects', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => false,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'irene_projects', $args );

}

// Hook into the 'init' action
add_action( 'init', 'set_irene_projects', 0 );



function generate_projects( $atts, $cat = '' ) {
	$output = '';
	$output.= '<div id="projects-wrapper">';
	$cat = $atts['cat'];
	$args = array(
		'post_type'			=>	'irene_projects',
		'posts_per_page'	=>	-1,
		'category_name'		=>	$cat,
		'orderby'			=>	'menu_order',
		'order'				=>	'ASC'
	);
    query_posts( $args );
	while ( have_posts() ) : the_post();
		$output.= '<div class="project"><a href="'.get_the_permalink().'"><div class="project-image"><div class="project-image-overlay"></div>';
	    if ( has_post_thumbnail() ) {
			$output.= get_the_post_thumbnail(null, 'small-square');
		} 
		$output.= '</div>';
	    $output.= '<div class="project-text"><h3>';
	    $output.= get_the_title();
	    $output.= '</h3><p>'.get_the_excerpt().'</p>';
	    $output.= '</div></a></div>';
	endwhile;
	wp_reset_query();
	$output.= '</div>';
	return $output;
}
add_shortcode( 'projects', 'generate_projects' );



function generate_blog( $atts ) {
	echo '<div id="blog-wrapper">';
    query_posts('post_type=post&posts_per_page=-1');
	while ( have_posts() ) : the_post();
		echo '<div class="blog-post"><div class="blog-image">';
	    if ( has_post_thumbnail() ) {
			the_post_thumbnail('small-square');
		} 
		echo '</div>';
	    echo '<div class="blog-title"><h3><a href="'.get_the_permalink().'">';
	    the_title();
	    echo '</a></h3><p class="blog-excerpt">'.get_the_excerpt().'</p><p class="blog-date">'.get_the_date().'</p>';
	    echo '</div></div>';
	endwhile;
	wp_reset_query();
	echo '</div>';
}
add_shortcode( 'blog_posts', 'generate_blog' );

function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


function siteorigin_universal_color_pallet() {
?>
    <script>
        jQuery(document).ready(function($){
            $.wp.wpColorPicker.prototype.options = {
                palettes: ['#ffffff', '#000000','#BA2B93', '#292927','#09B2B2', '#FFDA19', '#C9C3C4']
            };
        });
    </script>
<?php
}
add_action('admin_print_footer_scripts', 'siteorigin_universal_color_pallet');
add_action('customize_controls_print_footer_scripts', 'siteorigin_universal_color_pallet');

?>