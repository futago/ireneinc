<?php get_header(); ?>
            <article id="main-content" class="project-page page">
              <div class="page-top">
                <h1><span class="pink">Places</span> / <?php echo the_title(); ?></h1>
                <div class="page-top-image">
                  <?php 
                    if ( has_post_thumbnail() ) {
                      the_post_thumbnail();
                    } 
                  ?>
                </div>
              </div>
              <p class="category">
                <?php $my_cats = get_the_category();
                  $num = count($my_cats);
                  $c = 0;
                  foreach( $my_cats as $my_cat ) {
                    $c++;
                    echo $my_cat->cat_name;
                    if( $c < $num ) echo ', ';
                  }
                ?>
              </p>
              <a class="button backlink" href="<?php echo get_the_permalink( 33 ); ?>">Explore more projects</a>
              <h2 class="title"><?php echo the_title(); ?></h2>
              <div class="outer">
                <div class="left">
                  <div class="project-content">
                    <?php
                        if (have_posts()) :
                           while (have_posts()) :
                              the_post();
                                 the_content();
                           endwhile;
                        endif;
                    ?>
                  </div>
                  <div class="navigation">
                    <?php
                    $prev_post = get_previous_post();
                    if (!empty( $prev_post )): ?>
                    <div>
                      <a href="<?php echo $prev_post->url ?>" class="prev-nav"><?php echo $prev_post->post_title ?></a>
                    </div>
                    <?php endif ?>
                    <?php
                    $next_post = get_next_post();
                    if (!empty( $next_post )): ?>
                    <div>
                      <a href="<?php echo $next_post->url ?>" class="next-nav"><?php echo $next_post->post_title ?></a>
                    </div>
                    <?php endif ?>
                </div>
                </div>
                <div class="right">
                  <div id="projects-sidebar">
                    <?php

                      if( have_rows('related_projects') ):

                        while ( have_rows('related_projects') ) : the_row();

                            $post = get_sub_field('related_project');
                            ?>
                              <div class="project">
                                <a href="<?php echo get_permalink($post->ID); ?>">
                                <div class="project-image"><div class="project-image-overlay"></div>
                                <?php if(has_post_thumbnail($post->ID) ){
                                  echo get_the_post_thumbnail( $post->ID, 'small-square' ); 
                                } ?>
                                </div>
                                <div class="project-text">
                                  <h3><?php the_title($post->ID); ?></h3>
                                  <p><?php echo get_the_excerpt($post->ID) ?></p>
                                </div>
                                </a>
                              </div>
                            <?php
                            wp_reset_postdata();

                        endwhile;



                        endif;
                    ?>
                    </div>
                  </div>
                </div>
            </article>
            
<?php get_footer(); ?>