(function ($, root, undefined) {

$('#mobile-menu-link').click(function(e){
	e.preventDefault();
	$('#menu').addClass('active');
});

$('#menu .close').click(function(e){
	e.preventDefault();
	$('#menu').removeClass('active');
});

function checkImages(){

	$('article img').each(function(){
		var image = $(this);
		var container = image.parent();
		var imageRatio = image.width() / image.height();
		var containerRatio = container.width() / container.height();
		if (imageRatio > containerRatio){
			image.removeClass('wider taller equal').addClass('wider');
		}
		else if (imageRatio < containerRatio){
			image.removeClass('wider taller equal').addClass('taller');
		}
		else{
			image.removeClass('wider taller equal').addClass('equal');
		}
	});

}

$('.buttons img').each(function(){
	$(this).appendTo($(this).closest('.sow-image-container'));
});

checkImages();

$(window).resize(function(){
	checkImages();
});

})(jQuery, this);